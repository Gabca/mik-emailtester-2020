#ifndef CHECKER_H_INCLUDED
#define CHECKER_H_INCLUDED
int checkEmailIsValid(char emailString[]);

int checkResult(int resultCode);

int isCharDotOrAt(char current);
#endif // CHECKER_H_INCLUDED
