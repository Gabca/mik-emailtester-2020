#include <stdio.h>
#include <stdlib.h>
#include "checker.h"


int checkEmailIsValid(char emailString[]){
 if(emailString != NULL){
    int emailLength = strlen(emailString);

    char atChar = '@';
    char dotChar = '.';

    int countOfDot = 0;
    int countOfAt = 0;

    for (int i=0; i < emailLength; i++){
        char currentChar = emailString[i];
        char nextChar = emailString[i+1];
        if(currentChar == atChar){
            countOfAt++;
        }
        if(countOfAt > 1){
        return 3;
    }
        if(currentChar == dotChar){
            countOfDot++;
        }

        if((i == 0 || i == emailLength -1) && isCharDotOrAt(currentChar)){
            return 1;
        }
        if(nextChar != NULL && isCharDotOrAt(currentChar) == 1 && isCharDotOrAt(nextChar) == 1){
            return 2;
        }
    }

    if(countOfAt > 0){
        return 3;
    }

    if(countOfDot < 1){
        return 4;
    }

    }
    return 0;
}

int isCharDotOrAt(char current){
    char atChar = '@';
    char dotChar = '.';
    if(current != NULL &&(current == atChar || current == dotChar)){
        return 1;
    }
    return 0;
}

int checkResult(int resultCode){
    switch(resultCode){
        case 1:
            printf("The emails first or last char cannot be a '.' or an '@'!");
            break;
        case 2:
            printf("Cannot be a '.' beside an '@' or a '.' beside a '.' or an '@' beside an '@'!");
            break;
        case 3:
            printf("Email must have contains exactly one '@'!");
            break;
        case 4:
            printf("Email must have at least one '.'!");
            break;
        default:
            printf("Email is valid!");
        }
    return 0;
}
