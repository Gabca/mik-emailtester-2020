#include <stdio.h>
#include <stdlib.h>
#include "checker.h"

int main(int argc,char *argv[]){
    // kiszedjük az email paramétert
    char *email = argv[1];

    int result = checkEmailIsValid(email);

    return checkResult(result);
}
